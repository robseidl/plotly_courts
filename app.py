import os
from random import randint
import flask

import dash
import dash_core_components as dcc
import dash_html_components as html

import plotly.graph_objs as go
import numpy as np
import pandas as pd
import dill

from sklearn.decomposition import PCA
from sklearn.datasets import make_classification



print(dcc.__version__) # 0.6.0 or above is required

# Load data

#df = pd.read_csv(
#    'https://raw.githubusercontent.com/plotly/'
#    'datasets/master/gapminderDataFiveYear.csv')



# Generate some data
X, y = make_classification(n_classes=2, class_sep=2, weights=[0.1, 0.9],
                           n_informative=3, n_redundant=1, flip_y=0,
                           n_features=20, n_clusters_per_class=1,
                           n_samples=100, random_state=10)

# Instantiate a PCA object for the sake of easy visualisation
pca = PCA(n_components = 2)

# Fit and transform x to visualise inside a 2D feature space
x_vis = pca.fit_transform(X)

player_names_0 = ["Team 0: Player {}".format(i) for i,_ in enumerate(x_vis[y==0, 1])]
player_names_1 = ["Team 1: Player {}".format(i) for i,_ in enumerate(x_vis[y==1, 1])]

df = dill.load(open('data/video_data.dill',"rb"))

# Plot the original data
# Plot the two classes
xdim = 105
ydim = 68

left_offset = 0
right_offset = xdim
left_offset_delta = 5
right_offset_delta = -5
delta = 8

annotations = []

init_data = df[650]
ball_data = init_data.iloc[0]

player_positions = init_data[['x', 'y']].as_matrix()
for i, player_pos in enumerate(player_positions):
    if i != 0:
        color = '#ff7f0e' if i < 16 else '#0099cc'
        if not np.isnan(player_pos).any():
            annotations.append(go.Annotation(
                x=player_pos[0],
                y=player_pos[1],
                xref='x',
                yref='y',
                #xanchor='left',
                #yanchor='top',
                text=str(i - 1),
                showarrow=False,
                font=dict(
                    family='Courier New, monospace',
                    size=16,
                    color='#ffffff'
                ),
                bordercolor='#c7c7c7',
                borderwidth=2,
                borderpad=2,
                bgcolor=color,
                opacity=0.8

            ))
            #print(str(i - 1), (player_pos[0] - 1.2, player_pos[1] - 1.2))

        else:
            team = init_data.iloc[i]['team_id']
            this_offset = left_offset_delta if init_data.iloc[i]['team_id'] == 'H' else right_offset_delta
            border = left_offset if init_data.iloc[i]['team_id'] == 'H' else right_offset

            annotations.append(go.Annotation(
                x=border + this_offset,
                y=-5,
                xref='x',
                yref='y',
                #xanchor='left',
                #yanchor='top',
                text=str(i - 1),
                showarrow=False,
                font=dict(
                    family='Courier New, monospace',
                    size=16,
                    color='#ffffff'
                ),
                bordercolor='#c7c7c7',
                borderwidth=2,
                borderpad=2,
                bgcolor=color,
                opacity=0.8

            ))

            if team == 'H':
                left_offset_delta = left_offset_delta + delta
            else:
                right_offset_delta = right_offset_delta - delta



data = []

trace1 = go.Scatter(
    showlegend=False,
    x=[xdim/2, xdim/2],
    y=[ydim, 0],
    mode='lines',
    line=go.Line(width=2, color='white')
)
data.append(trace1)
trace2 = go.Scatter(
    showlegend=False,
    x=[0, 16.5, 16.5, 0],
    y=[54, 54, 14, 14],
    mode='lines',
    line=go.Line(width=2, color='white'),

)
data.append(trace2)
trace3 = go.Scatter(
    showlegend=False,
    x=[105, 105-16.5, 105-16.5, 105],
    y=[54, 54, 14, 14],
    mode='lines',
    line=go.Line(width=2, color='white')
)
data.append(trace3)


trace4 = go.Scatter(
    showlegend=False,
    x=[0, -3, -3, 0],
    y=[30.35, 30.35, 37.7, 37.7],
    mode='lines',
    line=go.Line(width=2, color='white')
)
data.append(trace4)

trace5 = go.Scatter(
    showlegend=False,
    x=[105, 108, 108, 105],
    y=[30.35, 30.35, 37.7, 37.7],
    mode='lines',
    line=go.Line(width=2, color='white')
)
data.append(trace5)

trace6 = go.Scatter(
    showlegend=False,
    x=[ball_data['x']],
    y=[ball_data['y']],
    mode='markers',
    marker=dict(
                size=14,
                color='black',
                opacity= 0.9
                )
    #line=go.Line(width=2, color='white')
)
data.append(trace6)

#ax.scatter(ball_data['x'], ball_data['y'], s=100, c='k')

print(ball_data['x'], ball_data['y'])

'''data.append(go.Scatter(x = ball_data['x'], y=ball_data['y'],mode='markers',
                    marker= dict(size= 44,
                                opacity= 0.7
                            ),
                    name="Ball"))'''

#data.append(go.Scatter(x = x_vis[y==0, 0], y = x_vis[y==0, 1], customdata= player_names_0, mode='markers', marker= dict(size= 16,
#                    opacity= 0.7
#                   ), name="Class #0"))
#data.append(go.Scatter(x = x_vis[y==1, 0], y = x_vis[y==1, 1], customdata = player_names_1, mode='markers', marker= dict(size= 16,
#                    opacity= 0.7
#                   ),name="Class #1"))

'''layout = dict(title = 'Dashboard Visualization',
              yaxis = dict(zeroline = False),
              xaxis = dict(zeroline = False),
               showlegend= False
             )'''

layout = go.Layout(
    title='Dashboard Visualization',
    hovermode='closest',
    autosize=True,
    width=600,
    height=450,
    plot_bgcolor='green',
    xaxis=go.XAxis(
        range=[-5, xdim+5],
        showgrid=False,
        showticklabels=False,
        zeroline=False,
        fixedrange=True
    ),
    yaxis=go.YAxis(
        range=[-5,ydim+5],
        showgrid=False,
        showticklabels=False,
        zeroline=False,
        fixedrange=True
    ),
    shapes=[
        # unfilled Rectangle
        {
            'type': 'rect',
            'x0': 0,
            'y0': 0,
            'x1': 105,
            'y1': 68,
            'line': {
                'color': 'white'
            }
        }],
    annotations=go.Annotations( annotations )



)

#### Tennis data

height_court = 10.97
width_court = 11.89*2
service_box = 6.4
double_field = 1.37

tennis_annotations = [
    go.Annotation(
                x=3.5,
                y=-3,
                xref='x',
                yref='y',
                text="RF",
                showarrow=False,
                font=dict(
                    family='Courier New, monospace',
                    size=16,
                    color='#ffffff'
                ),
                bordercolor='#c7c7c7',
                borderwidth=2,
                borderpad=2,
                bgcolor='#ff7f0e',
                opacity=0.8

            ),
    go.Annotation(
        x=7,
        y=width_court+2,
        xref='x',
        yref='y',
        text="RN",
        showarrow=False,
        font=dict(
            family='Courier New, monospace',
            size=16,
            color='#ffffff'
        ),
        bordercolor='#c7c7c7',
        borderwidth=2,
        borderpad=2,
        bgcolor='#0099cc',
        opacity=0.8

    )
]

tennis_data = []

# net
trace1 = go.Scatter(
    showlegend=False,
    y=[width_court/2, width_court/2],
    x=[height_court, 0],
    mode='lines',
    line=go.Line(width=2, color='white')
)
tennis_data.append(trace1)
trace2 = go.Scatter(
    showlegend=False,
    y=[width_court/2 - service_box, width_court/2 + service_box, width_court/2 + service_box, width_court/2 - service_box,width_court/2 - service_box],
    x=[height_court-double_field, height_court-double_field, 0+double_field, 0+double_field,height_court-double_field],
    mode='lines',
    line=go.Line(width=2, color='white'),

)
tennis_data.append(trace2)

trace3 = go.Scatter(
    showlegend=False,
    y=[width_court/2 - service_box, width_court/2 + service_box],
    x=[height_court/2, height_court/2],
    mode='lines',
    line=go.Line(width=2, color='white')
)
tennis_data.append(trace3)


trace4 = go.Scatter(
    showlegend=False,
    y=[0, 0 + 0.45],
    x=[height_court/2, height_court/2],
    mode='lines',
    line=go.Line(width=2, color='white')
)
tennis_data.append(trace4)


trace5 = go.Scatter(
    showlegend=False,
    y=[width_court, width_court - 0.45],
    x=[height_court/2, height_court/2],
    mode='lines',
    line=go.Line(width=2, color='white')
)
tennis_data.append(trace5)

trace6 = go.Scatter(
    showlegend=False,
    y=[0, width_court],
    x=[1.37, 1.37],
    mode='lines',
    line=go.Line(width=2, color='white')
)
tennis_data.append(trace6)

trace7 = go.Scatter(
    showlegend=False,
    y=[0, width_court],
    x=[height_court - 1.37, height_court - 1.37],
    mode='lines',
    line=go.Line(width=2, color='white')
)
tennis_data.append(trace7)

trace8 = go.Scatter(
    showlegend=False,
    x=[5],
    y=[4],
    mode='markers',
    marker=dict(
                size=14,
                color='yellow',
                opacity= 0.9
                )
)
tennis_data.append(trace8)


tennis_layout = go.Layout(
    title='Tennis court',
    hovermode='closest',
    autosize=True,
    width=450,
    height=600,
    plot_bgcolor='orange',
    yaxis=go.XAxis(
        range=[-5, width_court+5],
        showgrid=False,
        showticklabels=False,
        zeroline=False,
        fixedrange=True
    ),
    xaxis=go.YAxis(
        range=[-0.5,height_court+0.5],
        showgrid=False,
        showticklabels=False,
        zeroline=False,
        fixedrange=True
    ),
    shapes=[
        # unfilled Rectangle
        {
            'type': 'rect',
            'y0': 0,
            'x0': 0,
            'y1': 23.78,
            'x1': 10.97,
            'line': {
                'color': 'white'
            }
        }],
    annotations=go.Annotations( tennis_annotations )



)


####


# Setup the app
# Make sure not to change this file name or the variable names below,
# the template is configured to execute 'server' on 'app.py'
server = flask.Flask(__name__)
server.secret_key = os.environ.get('secret_key', str(randint(0, 1000000)))
app = dash.Dash(__name__, server=server)

# Since we're adding callbacks to elements that don't exist in the app.layout,
# Dash will raise an exception to warn us that we might be
# doing something wrong.
# In this case, we're adding the elements through a callback, so we can ignore
# the exception.
app.config.supress_callback_exceptions = True

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


index_page  = html.Div([
    html.H1('Dash multiple routes'),
    html.H2('Heroku example'),
    dcc.Link('Go to Football', href='/football'),
    html.Br(),
    dcc.Link('Go to Tennis', href='/tennis'),
])

football_page = html.Div(className="container", children=[
    html.H1('Football dashboard'),
    html.Div(className="row", children=[
        html.Div(className="nine columns",children=[

    dcc.Graph(
        id='footballData',
        figure={
            "config":{"displayModeBar": False},
            'data': data,
            'layout': layout
        }
    )]), html.Div(className="three columns",children=[
            html.H2('Player info'),
            html.Div(
                html.Pre(id='click-data')
            )
        ])
    ]),
    html.Div(className="row", children=[
        html.Br(),
        dcc.Link('Go to Tennis', href='/tennis'),
        html.Br(),
        dcc.Link('Go back to home', href='/')
    ])
])


import json

@app.callback(
    dash.dependencies.Output('click-data', 'children'),
    [dash.dependencies.Input('footballData', 'clickData')])
def display_hover_data(clickData):
    data = [clickData['points'][0]['customdata'],
            (clickData['points'][0]['x'],
             clickData['points'][0]['y'])]
    print(data)
    return json.dumps(clickData, indent=2)

tennis_page = html.Div(className="container", children=[
    html.H1('Tennis court'),
    html.Div(className="row", children=[
        html.Div(className="nine columns",children=[

    dcc.Graph(
        id='tennisData',
        figure={
            "config":{"displayModeBar": False},
            'data': tennis_data,
            'layout': tennis_layout
        }
    )]), html.Div(className="three columns",children=[
            html.H2('Player info'),
            html.Div(
                html.Pre(id='click-data')
            )
        ])
    ]),
    html.Div(className="row", children=[
        html.Br(),
        dcc.Link('Go to Football', href='/football'),
        html.Br(),
        dcc.Link('Go back to home', href='/')
    ])
])


# Update the index
@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/football':
        return football_page
    elif pathname == '/tennis':
        return tennis_page
    else:
        return index_page
    # You could also return a 404 "URL not found" page here

app.css.append_css({
    'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'
})


# Run the Dash app
if __name__ == '__main__':
    app.server.run(debug=True,threaded=True)